// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifndef WIN32_LEAN_AND_MEAN
 #define WIN32_LEAN_AND_MEAN
#endif // ifndef WIN32_LEAN_AND_MEAN
#include <iterator>
#include <Windows.h>
#include <EuroScopePlugIn.h>

using std::string;

class ES_ListType;

// class ES_Iterator
// {
// public:
// ES_Iterator(ES_ListType);
// virtual ~ES_Iterator(void);
// };

template<class T> class ES_Iterator
{
public:
	ES_Iterator(T);
	virtual ~ES_Iterator();

private:
	T data;
};

class ES_ListType
{
public:
	ES_ListType();
	virtual ~ES_ListType();
};

class ES_RadarTarget
	: public ES_ListType
{
public:
	ES_RadarTarget();
	~ES_RadarTarget();

private:
	EuroScopePlugIn::CRadarTarget *radartarget;
};

class ES_FlightPlan
	: public ES_ListType
{
public:
	ES_FlightPlan();
	~ES_FlightPlan();

	EuroScopePlugIn::CFlightPlan *flightplan;
};

class ES_Controller
	: public ES_ListType
{
public:
	ES_Controller();
	~ES_Controller();
};
