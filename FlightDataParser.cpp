// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "FlightDataParser.h"

FlightDataParser::FlightDataParser(CPlugIn *pPlugIn, Settings *pSettings)
	: _pPlugIn(pPlugIn), _settings(pSettings) {}

FlightDataParser::~FlightDataParser() {}

void FlightDataParser::ParseToJson(CFlightPlan &flightPlan, JsonBox::Array &FlightDataArray)
{
	CRadarTargetPositionData position;
	static JsonBox::Object FlightObject;

	FlightObject.clear();
	FlightObject["Callsign"] = JsonBox::Value(flightPlan.GetCallsign());
	GetFlightPlanData(flightPlan, FlightObject, &position);
	GetRadarData(position, FlightObject);
	FlightDataArray.push_back(FlightObject);
}

void FlightDataParser::ParseToJson(CRadarTarget &radarTarget, JsonBox::Array &FlightDataArray)
{
	static JsonBox::Object FlightObject;

	FlightObject.clear();
	FlightObject["Callsign"] = radarTarget.GetCallsign();
	GetRadarData(radarTarget.GetPosition(), FlightObject);
	FlightDataArray.push_back(FlightObject);
}

void FlightDataParser::GetFlightPlanData(CFlightPlan &flightPlan, JsonBox::Object &FlightObject, CRadarTargetPositionData *pPosition)
{
	CRadarTarget radarTarget = _pPlugIn->RadarTargetSelect(flightPlan.GetCallsign());

	if (radarTarget.IsValid())
		*pPosition = radarTarget.GetPosition();
	else
		*pPosition = flightPlan.GetFPTrackPosition();

	CFlightPlanData FlightPlanData = flightPlan.GetFlightPlanData();
	FlightObject["AcInfo"]      = FilterInvalidChars(FlightPlanData.GetAircraftInfo());
	FlightObject["Origin"]      = FilterInvalidChars(FlightPlanData.GetOrigin());
	FlightObject["Destination"] = FilterInvalidChars(FlightPlanData.GetDestination());
	FlightObject["Remarks"]     = FilterInvalidChars(FlightPlanData.GetRemarks());
	FlightObject["Route"]       = FilterInvalidChars(FlightPlanData.GetRoute());
	FlightObject["Prediction"]  = GetPredictionString(flightPlan);
	FlightObject["Scratch"]     = FilterInvalidChars(flightPlan.GetControllerAssignedData().GetScratchPadString());
	FlightObject["Controller"]  = flightPlan.GetTrackingControllerCallsign();
}

void FlightDataParser::GetRadarData(CRadarTargetPositionData &position, JsonBox::Object &FlightObject)
{
	static size_t size = FlightObject.size();

	FlightObject["Latitude"]  = position.GetPosition().m_Latitude;
	FlightObject["Longitude"] = position.GetPosition().m_Longitude;
	FlightObject["Altitude"]  = position.GetPressureAltitude();
	FlightObject["Speed"]     = position.GetReportedGS();
	FlightObject["Heading"]   = position.GetReportedHeading();
	if (size == 1)
		FillEmptyValues(FlightObject);
}

void FlightDataParser::FillEmptyValues(JsonBox::Object &FlightObject)
{
	FlightObject["AcInfo"]      = JsonBox::Value();
	FlightObject["Origin"]      = JsonBox::Value();
	FlightObject["Destination"] = JsonBox::Value();
	FlightObject["Remarks"]     = JsonBox::Value();
	FlightObject["Route"]       = JsonBox::Value();
	FlightObject["Prediction"]  = JsonBox::Value();
	FlightObject["Scratch"]     = JsonBox::Value();
	FlightObject["Controller"]  = JsonBox::Value();
}

string& FlightDataParser::GetPredictionString(const CFlightPlan &flightPlan)
{
	static string routePrediction;
	bool sendPassed = (_settings->Get(Settings::SET_PASSED_FIX) == "0" ? false : true);
	int timeToFix;
	CFlightPlanExtractedRoute extractedRoute = flightPlan.GetExtractedRoute();
	int size = extractedRoute.GetPointsNumber();

	routePrediction.clear();
	for (int i = 1; i < size; ++i)
	{
		if ((timeToFix = extractedRoute.GetPointDistanceInMinutes(i)) == -1 && !sendPassed)
			continue;
		if (routePrediction.size())
			routePrediction.push_back(' ');
		routePrediction.append(extractedRoute.GetPointName(i));
		routePrediction.push_back('[');
		routePrediction.append(_converter.ToString(timeToFix));
		routePrediction.push_back(']');
	}
	return routePrediction;
}

string& FlightDataParser::FilterInvalidChars(const string &str)
{
	static string newString;
	static locale loc = locale();

	newString.clear();
	for (string::const_iterator it = str.begin(); it != str.end(); ++it)
	{
		if (*it == '&')
			newString.append("%26");
		else if (std::isgraph(*it, loc))
			newString.push_back(*it);
		else
			newString.push_back(' ');
	}
	return newString;
}
