// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <locale>
#include <string>
#include <EuroScopePlugIn.h>
#include <JsonBox.h>
#include "Settings.h"
#include "converter.h"

using namespace EuroScopePlugIn;
using std::string;

class FlightDataParser
{
public:
	FlightDataParser(CPlugIn *pPlugIn, Settings *pSettings);
	virtual ~FlightDataParser();
	void ParseToJson(CFlightPlan &flightPlan, JsonBox::Array &FlightDataArray);
	void ParseToJson(CRadarTarget &radarTarget, JsonBox::Array &FlightDataArray);

private:
	CPlugIn *_pPlugIn;
	Settings *_settings;
	Converter _converter;
	void GetFlightPlanData(CFlightPlan &flightPlan, JsonBox::Object &FlightObject, CRadarTargetPositionData *position);
	void GetRadarData(CRadarTargetPositionData &position, JsonBox::Object &FlightObject);
	void FillEmptyValues(JsonBox::Object &FlightObject);
	string& GetPredictionString(const CFlightPlan &flightPlan);
	string& FilterInvalidChars(const string &str);
};
