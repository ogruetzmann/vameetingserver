// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once
#ifdef _DEBUG
 #include <fstream>
#endif // ifdef _DEBUG

#include <string>
#include <time.h>
#include <EuroScopePlugIn.h>
#include <JsonBox.h>
#include "socket.h"
#include "Settings.h"
#include "SettingsDialog.h"
#include "converter.h"
#include "FlightDataParser.h"

const char NAME[]         = "VAMeetingServer";
const char DEVELOPER[]    = "Oliver Gruetzmann";
const char VERSION[]      = "0.61";
const char LICENSE[]      = "GPL v3";
const char CHAT_HANDLER[] = "VAMeetingServer";

using namespace EuroScopePlugIn;
using std::string;

class CVAMeetingServer
	: public EuroScopePlugIn::CPlugIn
{
	time_t _lastUpdate;
	Converter _conv;
	Socket *_socket;
	Settings *_settings;

public:
	CVAMeetingServer(void);
	virtual ~CVAMeetingServer(void);
	string GetPredictionString(const CFlightPlanExtractedRoute &exRoute);
	void OnRadarTargetPositionUpdate(CRadarTarget RadarTarget);
	bool OnCompileCommand(const char *sCommandLind);

private:
	void ProcessData();
	CPosition* GetAirportPosition(string icao) const;
	double GetDistanceFromAirport(CPosition &acPosition, CPosition *apPosition) const;
	bool IcaoFilter(CFlightPlan &fp) const;
	int SendData(string data, bool retry = false);
	void GetFlightPlanData(CFlightPlan &FlightPlan, JsonBox::Object &object);
	void GetAircraftData(CRadarTarget &RadarTarget, JsonBox::Object &object);
	int GetTimeStamp();
	void InitVariables();
};
