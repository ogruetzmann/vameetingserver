#include "SocketException.h"

SocketException::SocketException(int error_code, const char *error_string)
	: _error_code(error_code), _error_string(error_string) {}

SocketException::~SocketException() {}

const char* SocketException::what() const
{
	ostringstream ostr;

	ostr.str("Socket Error! Winsock Error Code: ");
	ostr << _error_code;
	if (_error_string)
		ostr << " | " << _error_string;
	return ostr.str().c_str();
}

SocketException::SocketException() {}
