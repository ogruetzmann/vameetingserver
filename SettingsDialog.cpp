// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "SettingsDialog.h"

SettingsDialog::SettingsDialog(Settings *settings)
{
	OpenDialog(settings);
}

SettingsDialog::~SettingsDialog()
{
	if (_hGlobal)
		FreeResource(_hGlobal);
}

void SettingsDialog::OpenDialog(Settings *settings)
{
	HRSRC hrsrc = FindResource(GetModuleHandle("VAMeetingServer.dll"), MAKEINTRESOURCE(IDD_DIALOG1), RT_DIALOG);
	HWND parent = GetForegroundWindow();

	_hGlobal = LoadResource(GetModuleHandle("VAMeetingServer.dll"), hrsrc);

	CreateDialogIndirectParam(GetModuleHandle("VAMeetingServer.dll"), LPCDLGTEMPLATE(_hGlobal), parent, SettingsDialogProc, LPARAM(settings));
}

INT_PTR CALLBACK SettingsDialog::SettingsDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Settings *_settings;

	switch (message)
	{
	case WM_INITDIALOG:
		_settings = reinterpret_cast<Settings*>(lParam);
		SetDlgItemText(hDlg, IDC_ARRAPT, _settings->Get(Settings::SET_ARRAPT).c_str());
		SetDlgItemText(hDlg, IDC_DEPAPT, _settings->Get(Settings::SET_DEPAPT).c_str());
		SetDlgItemText(hDlg, IDC_SERVER, _settings->Get(Settings::SET_SERVER).c_str());
		SetDlgItemText(hDlg, IDC_PAGE, _settings->Get(Settings::SET_PAGE).c_str());
		SetDlgItemText(hDlg, IDC_USER, _settings->Get(Settings::SET_USER).c_str());
		SetDlgItemText(hDlg, IDC_PASSWORD, _settings->Get(Settings::SET_PASSWORD).c_str());
		SetDlgItemText(hDlg, IDC_APOFFSET, _settings->Get(Settings::SET_AP_OFFSET).c_str());
		SetDlgItemText(hDlg, IDC_REFRESH, _settings->Get(Settings::SET_INTERVAL).c_str());
		CheckDlgButton(hDlg, IDC_PASSED, (_settings->Get(Settings::SET_PASSED_FIX) == "0" ? BST_UNCHECKED : BST_CHECKED));
		CheckDlgButton(hDlg, IDC_POWERSWITCH, (_settings->isActive == true ? BST_CHECKED : BST_UNCHECKED));
		return INT_PTR(true);

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			const int BUFFER_SIZE = 100;
			char buffer[BUFFER_SIZE];

			GetDlgItemText(hDlg, IDC_ARRAPT, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_ARRAPT, buffer);
			GetDlgItemText(hDlg, IDC_DEPAPT, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_DEPAPT, buffer);
			GetDlgItemText(hDlg, IDC_SERVER, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_SERVER, buffer);
			GetDlgItemText(hDlg, IDC_PAGE, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_PAGE, buffer);
			GetDlgItemText(hDlg, IDC_USER, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_USER, buffer);
			GetDlgItemText(hDlg, IDC_PASSWORD, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_PASSWORD, buffer);
			GetDlgItemText(hDlg, IDC_APOFFSET, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_AP_OFFSET, buffer);
			GetDlgItemText(hDlg, IDC_REFRESH, buffer, BUFFER_SIZE);
			_settings->Set(Settings::SET_INTERVAL, buffer);
			_settings->isActive = (IsDlgButtonChecked(hDlg, IDC_POWERSWITCH) == BST_CHECKED ? true : false);
			_settings->Set(Settings::SET_PASSED_FIX, (IsDlgButtonChecked(hDlg, IDC_PASSED) == BST_CHECKED ? "1" : "0"));
			DestroyWindow(hDlg);
			_settings = 0;
			return INT_PTR(true);
		}
		if (LOWORD(wParam) == IDCANCEL)
		{
			DestroyWindow(hDlg);
			_settings = 0;
			return INT_PTR(true);
		}
	}
	return INT_PTR(false);
}
