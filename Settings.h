// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <EuroScopePlugIn.h>
#include "ESSettings.h"

class Settings
	: public ESSettings
{
public:
	Settings(EuroScopePlugIn::CPlugIn*);
	~Settings(void);

	static enum {SET_INTERVAL = 0, SET_AP_OFFSET, SET_SERVER, SET_PAGE, SET_USER, SET_PASSWORD, SET_PASSED_FIX, SET_ARRAPT, SET_DEPAPT} varIndex;

	bool isActive;

private:
	void InitVariables();
};
