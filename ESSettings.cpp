// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ESSettings.h"

ESSettings::ESSettings(EuroScopePlugIn::CPlugIn *pPlugIn)
	: _pPlugInHandle(pPlugIn), emptyString("") {}

ESSettings::~ESSettings(void) {}

const string& ESSettings::Get(int index) const
{
	const Variable &v = _varCon.GetVariable(index);

	if (v.IsValid())
		return LoadValue(v);
	return emptyString;
}

void ESSettings::Set(int index, string newValue) const
{
	Variable v = _varCon.GetVariable(index);

	if (v.IsValid())
		SaveValue(v, newValue);
}

void ESSettings::RegisterVariable(Variable &v)
{
	_varCon.push_back(v);
}

const string& ESSettings::LoadValue(const Variable &v) const
{
	static string sResult;
	const char *result = _pPlugInHandle->GetDataFromSettings(v.GetName().c_str());

	sResult.clear();
	if (result == 0)
	{
		SaveValue(v, v.GetValue());
		return v.GetValue();
	}
	return sResult = result;
}

void ESSettings::SaveValue(const Variable &v, const string &newValue) const
{
	_pPlugInHandle->SaveDataToSettings(v.GetName().c_str(), v.GetDescription().c_str(), newValue.c_str());
}
