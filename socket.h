// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// #ifndef WIN32_LEAN_AND_MEAN
// #define WIN32_LEAN_AND_MEAN
// #endif // ifndef WIN32_LEAN_AND_MEAN

#ifndef _MSC_VER
 #define WINVER 0x0501
#endif // ifndef _MSC_VER

#include <winsock2.h>
#include <string>
#include <sstream>
#include <exception>
#include <ws2tcpip.h>
#include <WinInet.h>
#include "converter.h"
#include "SocketException.h"

using std::string;
using std::exception;

const int METHOD_GET  = 1;
const int METHOD_POST = 2;

class Socket
{
public:
	Socket();
	virtual ~Socket();
	void Send(const string &host, const string &script, const string &data, int method = METHOD_GET, int port = 80, bool blocking = true);
	void Send(const string &host, const string &script, const string &data, string &response, int method = METHOD_GET, int port = 80, bool blocking = true);
	void Reset();
	void CreateSocket();
	void Connect(const string &host, int port = 80, bool blocking = true);
	void Disconnect();
	void SendData(const string &header) const;
	void Receive(string &result);

private:
	SOCKET _socket;
	WSADATA _wsadata;
	int instances;

	void StopWinsock();
	addrinfo* GetIpByHostName(const string &hostname, int port, int &err);
	void BuildHeader(const string &host, const string &script, const string &data, const char *prefix, int method, string &request_header) const;
};
