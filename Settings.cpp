// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Settings.h"

Settings::Settings(EuroScopePlugIn::CPlugIn *cPlug)
	: ESSettings(cPlug)
{
	InitVariables();
}

Settings::~Settings() {}

void Settings::InitVariables()
{
	RegisterVariable(Variable("SET_INTERVAL", "SET_INTERVAL", "10"));   // 0
	RegisterVariable(Variable("SET_AP_OFFSET", "SET_AP_OFFSET", "5"));  // 1
	RegisterVariable(Variable("SET_SERVER", "SET_SERVER", ""));         // 2
	RegisterVariable(Variable("SET_PAGE", "SET_PAGE", ""));             // 3
	RegisterVariable(Variable("SET_USER", "SET_USER", ""));       // 4
	RegisterVariable(Variable("SET_PASSWORD", "SET_PASSWORD", "")); // 5
	RegisterVariable(Variable("SET_PASSED_FIX", "SET_PASSED_FIX", "0"));    // 6
	RegisterVariable(Variable("SET_ARRAPT", "SET_ARRAPT", ""));   // 7
	RegisterVariable(Variable("SET_DEPAPT", "SET_DEPAPT", ""));   // 8

	isActive = false;
}
