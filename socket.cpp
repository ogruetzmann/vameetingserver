// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "socket.h"

Socket::Socket()
{
	instances = 0;
	int result = WSAStartup(MAKEWORD(2, 2), &_wsadata);
	if (result)
	{
		SocketException se(WSAGetLastError(), "Ctor()::WSAStartup()");
		_socket = INVALID_SOCKET;
		WSACleanup();
		throw se;
	}
	++instances;
}

Socket::~Socket()
{
	StopWinsock();
}

void Socket::Send(const string &host, const string &script, const string &data, int method, int port, bool blocking)
{
	try
	{
		CreateSocket();
		Connect(host, port, blocking);
		string header;
		BuildHeader(host, script, data, "data=", method, header);
		SendData(header);
		Disconnect();
	}
	catch (exception)
	{
		throw;
	}
	catch (...)
	{
		SocketException se(0, "Unknown Error");
		throw se;
	}
}

void Socket::Send(const string &host, const string &script, const string &data, string &response, int method, int port, bool blocking)
{
	try
	{
		CreateSocket();
		Connect(host, port, blocking);
		string header;
		BuildHeader(host, script, data, "data=", method, header);
		SendData(header);
		response = header + "@@@\n\n";
		Receive(response);
		Disconnect();
	}
	catch (exception)
	{
		throw;
	}
	catch (...)
	{
		SocketException se(0, "Unknown Error");
		throw se;
	}
}

void Socket::CreateSocket()
{
	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		SocketException se(WSAGetLastError(), "CreateSocket()::socket()");
		throw se;
	}
}

void Socket::Connect(const string &host, int port, bool blocking)
{
	int err      = 0;
	addrinfo *ai = GetIpByHostName(host, port, err);

	if (err)
	{
		SocketException se(WSAGetLastError(), "Connect()::GetIpByHostName()");
		throw se;
	}
	if (!blocking)
	{
		u_long iMode = 1L;
		ioctlsocket(_socket, FIONBIO, &iMode);
	}

	int res = connect(_socket, ai->ai_addr, sizeof(*(ai->ai_addr)));
	if (res && (WSAGetLastError() != WSAEWOULDBLOCK))
	{
		SocketException se(WSAGetLastError(), "Connect()::connect()");
		throw se;
	}
	freeaddrinfo(ai);
}

void Socket::Disconnect()
{
	int result = shutdown(_socket, SD_BOTH);

	if (result)
	{
		SocketException se(WSAGetLastError(), "Disconnect()::shutdown()");
		throw se;
	}
	result = closesocket(_socket);
	if (result)
	{
		SocketException se(WSAGetLastError(), "Disconnect()::closesocket()");
		throw se;
	}
}

void Socket::SendData(const string &header) const
{
	int result = send(_socket, header.c_str(), header.size()+1, 0);

	if (result == SOCKET_ERROR)
	{
		SocketException se(WSAGetLastError(), "SendData()::send()");
		throw se;
	}
}

void Socket::Receive(string &sResult)
{
	shutdown(_socket, SD_SEND);
	const int bufferSize = 1024;
	int result           = 0;
	char buf[bufferSize];
	int lErr = 0;
	do
	{
		result = recv(_socket, buf, bufferSize, 0);
		lErr   = WSAGetLastError();
		sResult.append(buf, result);
	} while (result > 0 || lErr == WSAEWOULDBLOCK);
}

void Socket::StopWinsock()
{
	if (_socket != INVALID_SOCKET)
	{
		shutdown(_socket, SD_BOTH);
		closesocket(_socket);
		_socket = INVALID_SOCKET;
	}
	if (instances <= 0)
		return;
	WSACleanup();
	--instances;
}

addrinfo* Socket::GetIpByHostName(const string &host, int port, int &err)
{
	addrinfo hints, *pOutput = 0;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	err = getaddrinfo(host.c_str(), Converter::ToString(port).c_str(), &hints, &pOutput);
	return pOutput;
}

void Socket::BuildHeader(const string &host, const string &script, const string &data, const char *prefix, int method, string &request_header) const
{
	Converter cc;
	string szData = cc.ToString(data.size() + strlen(prefix));

	request_header  = (method == METHOD_GET ? "GET " : "POST ") + script + " HTTP/1.1\n";
	request_header += "Host: " + host + "\n";
	request_header += "Content-Type: application/x-www-form-urlencoded\n";
	request_header += "Content-Length: " + szData + "\n\n";
	request_header += prefix;
	request_header += data;
}
