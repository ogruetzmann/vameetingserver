// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef IDC_STATIC
 #define IDC_STATIC (-1)
#endif // ifndef IDC_STATIC

#define IDD_DIALOG1 100
#define IDC_PASSED 1001
#define IDC_ARRAPT 1009
#define IDC_DEPAPT 1010
#define IDC_SERVER 1011
#define IDC_PAGE 1012
#define IDC_USER 1013
#define IDC_PASSWORD 1014
#define IDC_POWERSWITCH 1015
#define IDC_REFRESH 1018
#define IDC_APOFFSET 1019
