// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "VAMeetingServer.h"

CVAMeetingServer::CVAMeetingServer(void)
	: CPlugIn(COMPATIBILITY_CODE, NAME, VERSION, DEVELOPER, LICENSE)
{
	InitVariables();
	_settings = new Settings(this);
	_socket   = new Socket;
}

CVAMeetingServer::~CVAMeetingServer(void)
{
	delete _socket;
	delete _settings;
}

void CVAMeetingServer::OnRadarTargetPositionUpdate(CRadarTarget RadarTarget)
{
	double update = difftime(time(NULL), _lastUpdate);

	if (_settings->isActive && update > _conv.ToInt(_settings->Get(Settings::SET_INTERVAL)))
	{
		ProcessData();
		_lastUpdate = time(NULL);
	}
}

bool CVAMeetingServer::OnCompileCommand(const char *sCommandLine)
{
	if (!strcmp(sCommandLine, ".VAMS"))
	{
		SettingsDialog settingsDialog(_settings);
		return true;
	}
	return false;
}

void CVAMeetingServer::ProcessData()
{
	FlightDataParser parser(this, _settings);
	JsonBox::Object JsonBuffer;
	JsonBox::Array AircraftDataArray;

	JsonBuffer["Time"] = JsonBox::Value(GetTimeStamp());
	for (CFlightPlan fp = FlightPlanSelectFirst(); fp.IsValid(); fp = FlightPlanSelectNext(fp))
	{
		if (IcaoFilter(fp))
			continue;
		parser.ParseToJson(fp, AircraftDataArray);
	}

	CPosition *apPosition = GetAirportPosition(_settings->Get(Settings::SET_DEPAPT));
	for (CRadarTarget rt = RadarTargetSelectFirst(); rt.IsValid(); rt = RadarTargetSelectNext(rt))
	{
		CFlightPlan fp = FlightPlanSelect(rt.GetCallsign());
		if (fp.IsValid() && !IcaoFilter(fp))
			continue;

		CRadarTargetPositionData position = rt.GetPosition();
		double dist = GetDistanceFromAirport(position.GetPosition(), apPosition);
		if (dist == -1 || dist > _conv.ToInt(_settings->Get(Settings::SET_AP_OFFSET)) || position.GetReportedGS() > 50)
			continue;
		parser.ParseToJson(rt, AircraftDataArray);
	}
	JsonBuffer[ControllerMyself().GetCallsign()] = AircraftDataArray;
	ostringstream ostr;
	JsonBox::Value value;
	value = JsonBuffer;
	value.writeToStream(ostr, false);
	SendData(ostr.str());
	delete apPosition;
}

CPosition* CVAMeetingServer::GetAirportPosition(string icao) const
{
	CPosition apPosition;

	for (CSectorElement se = SectorFileElementSelectFirst(SECTOR_ELEMENT_AIRPORT);
	     se.IsValid();
	     se = SectorFileElementSelectNext(se, SECTOR_ELEMENT_AIRPORT))
		if (se.GetName() == icao && se.GetPosition(&apPosition, 0))
			return new CPosition(apPosition);
	return 0;
}

double CVAMeetingServer::GetDistanceFromAirport(CPosition &acPosition, CPosition *apPosition) const
{
	if (!apPosition)
		return -1;
	return acPosition.DistanceTo(*apPosition);
}

bool CVAMeetingServer::IcaoFilter(CFlightPlan &fp) const
{
	string departure   = _settings->Get(Settings::SET_DEPAPT);
	string destination = _settings->Get(Settings::SET_ARRAPT);

	// true if neither origin nor destination match
	if (destination.empty() && departure.empty())
		return false;
	return fp.GetFlightPlanData().GetOrigin() != departure && fp.GetFlightPlanData().GetDestination() != destination;
}

int CVAMeetingServer::SendData(string data, bool retry)
{
	string auth;

	auth  = "&auth=";
	auth += _settings->Get(Settings::SET_USER);
	auth += ",";
	auth += _settings->Get(Settings::SET_PASSWORD);
	data += auth;

	string host   = _settings->Get(Settings::SET_SERVER);
	string script = _settings->Get(Settings::SET_PAGE);
	if (script.size() && script.at(0) != '/')
		script.insert(0, 1, '/');

	string response;

	try
	{
		_socket->Send(host, script, data, response, METHOD_POST);
	}
	catch (exception &e)
	{
		DisplayUserMessage(CHAT_HANDLER, "Debug", e.what(), true, true, true, true, false);
	}

#ifdef _DEBUG
	std::ofstream file("file.txt");
	if (!file.is_open())
		return 0;
	file << data << std::endl << std::endl;
	file << response;
	file.close();
#endif // ifdef _DEBUG

	return 0;
}

int CVAMeetingServer::GetTimeStamp()
{
	tm time;

	gmtime_s(&time, &_lastUpdate);
	int timestamp = (time.tm_hour * 10000) + (time.tm_min * 100) + time.tm_sec;
	return timestamp;
}

void CVAMeetingServer::InitVariables()
{
	_lastUpdate = time(NULL);
}
