// VAMeetingServer PlugIn for EuroScope
// Copyright (C) 2012  Oliver Gruetzmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Variable.h"

Variable::Variable()
	: _description(""), _name(""), _value("") {}

Variable::Variable(string description, string name, string value)
	: _description(description), _name(name), _value(value) {}

const string& Variable::GetDescription() const
{
	return _description;
}

const string& Variable::GetName() const
{
	return _name;
}

const string& Variable::GetValue() const
{
	return _value;
}

bool Variable::IsValid() const
{
	if (_name.length())
		return true;
	return false;
}
